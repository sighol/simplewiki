import toml

def get_settings():
    with open("config.toml") as conffile:
        return toml.loads(conffile.read())


settings = get_settings()

WIKI_ROOT = settings["wiki_root"]
EDITOR_PATH = settings["editor_path"]