import unittest

import simplewiki.get_md_files as gmf

class test(unittest.TestCase):

    def test_md_files_prev_next(self):
        tree = [
            gmf.ViewGroup("group1", [
                "group1/md1-0",
                "group1/md1-1",
                "group1/md1-2",
                "group1/md1-3",
                "group1/md1-4",
            ]),
            gmf.ViewGroup("group2", [
                "group2/md2-0",
                "group2/md2-1",
                "group2/md2-2",
                "group2/md2-3",
                "group2/md2-4",
                "group2/md2-5",
            ]),
        ]

        group1 = tree[0]
        group2 = tree[1]

        (prev, next) = gmf.get_prev_next(tree, "group1/md1-0")
        self.assertEqual(prev, None)
        self.assertEqual(next, group1.list[1])

        (prev, next) = gmf.get_prev_next(tree, "group1/md1-3")
        self.assertEqual(prev, group1.list[2])
        self.assertEqual(next, group1.list[4])


        (prev, next) = gmf.get_prev_next(tree, "group1/md1-4")
        self.assertEqual(prev, group1.list[3])
        self.assertEqual(next, group2.list[0])

        (prev, next) = gmf.get_prev_next(tree, "group2/md2-0")
        self.assertEqual(prev, group1.list[4])
        self.assertEqual(next, group2.list[1])

        (prev, next) = gmf.get_prev_next(tree, "group2/md2-5")
        self.assertEqual(prev, group2.list[4])
        self.assertEqual(next, None)


if __name__ == "__main__":
    unittest.main()
