"""
Views for simplewiki
"""

import subprocess as sp
import os
from os.path import isfile, join

from flask import Flask, render_template, request, redirect, send_from_directory

import simplewiki.settings
from simplewiki import wiki_hierarchy
from simplewiki.markdown import markdown


app = Flask(__name__)

WIKI_ROOT = simplewiki.settings.get_settings()["wiki_root"]

def render(template_name, **kwargs):
    md_files = wiki_hierarchy.get_md_hierarchy()
    if "page" in kwargs.keys():
        (prev, next) = wiki_hierarchy.get_prev_next(md_files, kwargs["page"])
        if prev is not None:
            kwargs["previous_url"] = prev.file_name
        if next is not None:
            kwargs["next_url"] = next.file_name

    print(md_files)

    return render_template(template_name,
                           groups=md_files,
                           **kwargs)

def get_md_path(page):
    return join(WIKI_ROOT, page + ".md")


def get_abs_path(url):
    return join(WIKI_ROOT, url)


def page_exists(page):
    return isfile(get_md_path(page))


def get_file_contents(page):
    with open(get_md_path(page), "r", encoding="utf8") as file:
        return file.read()


def save_page(page, content):
    with open(get_md_path(page), "w", newline='', encoding="utf8") as file:
        file.write(content.replace("\r\n", "\n"))


def render_markdown(page):
    content = get_file_contents(page)
    md_content = markdown(content)
    return render(
        "show.html",
        body=md_content,
        title=page,
        page=page)




@app.route("/edit/<path:page>", methods=["GET", "POST"])
def edit(page):
    if request.method == "POST":
        print("Request recieved")
        content = request.form["content"]
        save_page(page, content)
        return redirect("/" + page)
    else:
        if page_exists(page):
            return render("edit.html",
                          content=get_file_contents(page),
                          page=page)
        else:
            return render("edit.html",
                          content="",
                          page=page)


@app.route("/edit_sublime/<path:page>", methods=["GET", "POST"])
def edit_sublime(page):
    file = get_md_path(page)
    editor = simplewiki.settings.EDITOR_PATH

    p = sp.Popen([editor, file])
    p.wait()
    return redirect("/" + page)


@app.route("/")
def show():
    return render("index.html")


def serve_static(file):
    dirname = os.path.dirname(file)
    filename = file.split("/")[-1]
    return send_from_directory(dirname, filename)


@app.route("/favicon.ico")
def favicon():
    return serve_static("static/favicon.ico")


@app.route("/<path:page>")
def show_page(page):
    file_path = get_md_path(page)
    if isfile(file_path):
        return render_markdown(page)
    image_path = get_abs_path(page)
    print(image_path)
    if isfile(image_path):
        return serve_static(image_path)
    return redirect("/edit/" + page)


if __name__ == "__main__":
    app.run(port=3030, threaded=True, debug=True)
