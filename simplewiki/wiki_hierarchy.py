import os
import itertools

from simplewiki.settings import WIKI_ROOT

class ViewGroup:

	def __init__(self, key, list):
		self.key = key.capitalize()
		self.list = [View(x, key) for x in list]
		self.list.sort(key=lambda x: x.name)

class View:
	def __init__(self, file_name, group_key):
		self.file_name = file_name
		self.name = file_name.replace(group_key + "/", "").capitalize().replace("-", " ")

	def __repr__(self):
		return "{{View file_name={}}}".format(self.file_name)

def get_md_hierarchy():
	md_files = []
	for root, dirs, files in  os.walk(WIKI_ROOT):
		relDir = os.path.relpath(root, WIKI_ROOT)
		md_files += [os.path.join(relDir, x).replace("\\", "/").replace(".md", "") for x in files if x.endswith(".md")]

	def get_key(x):
		split = x.split("/")
		if len(split) > 0:
			return split[0]
		return "unknown"

	out = []
	groups = itertools.groupby(md_files, get_key)
	for k, g in groups:
		out.append(ViewGroup(k, list(g)))

	out = [x for x in out if x.key != "."]
	out.sort(key=lambda x: x.key)
	return out

def get_prev_next(md_files, current_file_name):
	views = flatten([x.list for x in md_files])

	current = None

	for index in range(len(views)):
		prev = current
		current = views[index]
		next = None

		if index + 1 < len(views):
			next = views[index+1]

		if current.file_name == current_file_name:
			return (prev, next)

	return (None,None)


def flatten(listOfLists):
	"Flatten one level of nesting"
	return list(itertools.chain.from_iterable(listOfLists))