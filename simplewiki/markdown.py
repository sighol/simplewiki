
from markdown.extensions.wikilinks import WikiLinkExtension

import markdown as md
import markdown.extensions

def markdown(content):
	return md.markdown(
		content,
		extensions=[
			'markdown.extensions.codehilite',
			'markdown.extensions.fenced_code',
			'markdown.extensions.tables',
			WikiLinkExtension(base_url='/wiki/')
		])